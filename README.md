# NLP Applications to MBSE

Requirements for this project are in [requirements.txt](./requirements.txt)

## NER
Data cleaning and conversion to .spacy formats was done in [clean-goes-data.ipynb](./clean-goes-data.ipynb)

Training and tests were done in [train.ipynb](./train.ipynb)

## Matching
Spacy matching tests were completed in [SimilarityCheck.ipynb](./SimilarityCheck.ipynb)