backcall==0.2.0
blis==0.7.5
catalogue==2.0.6
certifi==2021.10.8
charset-normalizer==2.0.7
click==8.0.3
colorama==0.4.4
cymem==2.0.6
debugpy==1.5.1
decorator==5.1.0
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.2.0/en_core_web_sm-3.2.0-py3-none-any.whl
entrypoints==0.3
et-xmlfile==1.1.0
idna==3.3
ipykernel==6.5.1
ipython==7.29.0
jedi==0.18.1
Jinja2==3.0.3
joblib==1.1.0
jupyter-client==7.0.6
jupyter-core==4.9.1
langcodes==3.3.0
MarkupSafe==2.0.1
matplotlib-inline==0.1.3
murmurhash==1.0.6
nest-asyncio==1.5.1
numpy==1.21.4
openpyxl==3.0.9
packaging==21.3
pandas==1.3.4
parso==0.8.2
pathy==0.6.1
pickleshare==0.7.5
preshed==3.0.6
prompt-toolkit==3.0.22
pydantic==1.8.2
Pygments==2.10.0
pyparsing==3.0.6
python-dateutil==2.8.2
pytz==2021.3
pywin32==302
pyzmq==22.3.0
requests==2.26.0
scikit-learn==1.0.1
scipy==1.7.3
six==1.16.0
sklearn==0.0
smart-open==5.2.1
spacy==3.2.0
spacy-legacy==3.0.8
spacy-loggers==1.0.1
srsly==2.4.2
thinc==8.0.13
threadpoolctl==3.0.0
tornado==6.1
tqdm==4.62.3
traitlets==5.1.1
typer==0.4.0
typing_extensions==4.0.0
urllib3==1.26.7
wasabi==0.8.2
wcwidth==0.2.5
